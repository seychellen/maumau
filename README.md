## Docker
* Build your application with `gradle build` 
* Build your container with `docker build -t maumau .`
* Start the MauMau application with `docker-compose up -d`

## Alive check
* With `docker ps` you should see something like:
```
CONTAINER ID   IMAGE      COMMAND                  CREATED         STATUS                  PORTS                    NAMES
e5d627100345   maumau     "java -jar /app/MauM…"   8 seconds ago   Up 6 seconds            0.0.0.0:8080->8080/tcp   maumau-app-1
15187f9c3b47   postgres   "docker-entrypoint.s…"   3 weeks ago     Up 28 hours (healthy)   0.0.0.0:5432->5432/tcp   maumau-db-1

```
* With http://localhost:8080/ you should see the answer 'API-Schnittstelle von MauMau'