package net.kotlincook.plugins

import CreatePartieSpieler
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.webjars.*
import io.ktor.server.websocket.*
import io.ktor.util.logging.*
import io.ktor.websocket.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import net.kotlincook.model.CreateSpieler
import net.kotlincook.model.CreateSpielzug
import net.kotlincook.repo.ElementAlreadyExistsException
import net.kotlincook.repo.EntityNotExistsException
import net.kotlincook.repo.MauMauService
import org.jetbrains.exposed.dao.exceptions.EntityNotFoundException
import org.jetbrains.exposed.exceptions.ExposedSQLException
import java.util.*

internal val LOGGER = KtorSimpleLogger("net.kotlincook.plugins")

fun Application.configureRouting() {
//
//    val mapper = mapper<Spielzug, SpielzugDAO> {
//        autoMap(AutoMappingStrategy.BY_NAME)
//    }


    install(Webjars) {
        path = "/webjars" //defaults to /webjars
    }

    fun Throwable.infoText() = this.message ?: this.toString()

    install(StatusPages) {
        exception<Throwable> { call, e ->
            when (e) {
                is ExposedSQLException -> {
                    with(e.infoText()) {
                        when {
                            contains("violates unique constraint") ||
                                    contains("violates foreign key constraint") ->
                                call.respond(HttpStatusCode.Conflict, e.infoText())

                            else -> call.respond(HttpStatusCode.InternalServerError, e.infoText())
                        }
                    }
                }

                is EntityNotExistsException,
                is EntityNotFoundException -> {
                    call.respond(HttpStatusCode.NotFound, e.infoText())
                }

                is IllegalArgumentException -> {
                    call.respond(HttpStatusCode.BadRequest, e.infoText())
                }

                is IllegalStateException,
                is ElementAlreadyExistsException -> {
                    call.respond(HttpStatusCode.InternalServerError, e.infoText())
                }
            }
        }
    }

//    suspend fun CoroutineScope.handleExceptions(call: ApplicationCall, block: suspend CoroutineScope.() -> Unit) {
//        try {
//            block()
//        } catch (e: ExposedSQLException) {
//            ...
//        }
//    }

    fun uuidFromParameter(parameter: String?): UUID {
        return try {
            UUID.fromString(parameter)
        } catch (e: Exception) {
            throw IllegalArgumentException("Invalid ID ${parameter}")
        }
    }

    routing {
//        route("/api") {
        get("/") {
            call.respondText("API-Schnittstelle von MauMau")
        }

        get("/zustaende/{partieId}") {
            val partieId = uuidFromParameter(call.parameters["partieId"])
            val zustand = MauMauService.zustandVonPartie(partieId)
            call.respond(HttpStatusCode.OK, zustand)
        }

        val activeSessions = mutableListOf<DefaultWebSocketServerSession>()

        webSocket("/ws") { // websocketSession
            activeSessions.add(this)
            LOGGER.info("There are currently ${activeSessions.size} active sessions.")
            // Komischerweise muss man hier die incoming's durchiterieren, sonst klappt's nicht
            for (frame in incoming) {

                LOGGER.info("###################################################")
                LOGGER.info("Frame: $frame, active Sessions: ${activeSessions.size}")
                LOGGER.info("###################################################")
//                if (frame is Frame.Text) {
//                    val text = frame.readText()
//                    outgoing.send(Frame.Text("YOU SAID: $text"))
//                    if (text.equals("bye", ignoreCase = true)) {
//                        close(CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
//                    }
//                }
            }
        }

        post("/spielzuege") {

            val spielzug = call.receive<CreateSpielzug>()
            val zugnummer = MauMauService.macheSpielzug(spielzug)
            for (session in activeSessions) {
                try {
                    session.send("{\"test\":\"Vollmer\"}")
                } catch (ex: ClosedReceiveChannelException) {
                    println(ex)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
            call.respond(HttpStatusCode.Created, zugnummer)
        }

        get("/spielzuege") {
            val partieId = call.request.queryParameters["partieId"]
            val spielzuege = MauMauService.ladeSpielzuege(UUID.fromString(partieId))
            call.respond(HttpStatusCode.OK, spielzuege)
        }


        get("/ablagestapel") {
            val partieId = call.request.queryParameters["partieId"]
            val ablagestapel = MauMauService.ablagestapel(UUID.fromString(partieId))
            call.respond(HttpStatusCode.OK, ablagestapel)
        }

        // Create user
        post("/spieler") {
            val spieler = call.receive<CreateSpieler>()
            val id = MauMauService.erstelleSpieler(spieler)
            call.respond(HttpStatusCode.Created, id.toString())
        }

        // Read spieler
        get("/spieler/{id}") {
            val id = uuidFromParameter(call.parameters["id"])
            val spieler = MauMauService.ladeSpieler(id)
            call.respond(HttpStatusCode.OK, spieler)
        }

        get("/partien") {
            val partien = MauMauService.ladeAktivePartien()
            call.respond(HttpStatusCode.OK, partien)
        }

        // Create Spiel
        post("/partien") {
            val id = MauMauService.erstellePartie()
            call.respond(HttpStatusCode.Created, id.toString())
        }

        get("/partien/{id}") {
            val id = uuidFromParameter(call.parameters["id"])
            val partie = MauMauService.ladePartie(id)
            call.respond(HttpStatusCode.OK, partie)
        }

        get("/partien/{partieId}/spielerdaten/{spielerId}") {
            val partieId = uuidFromParameter(call.parameters["partieId"])
            val spielerId = uuidFromParameter(call.parameters["spielerId"])
            val spielerdaten = MauMauService.spielerdaten(partieId, spielerId)
            call.respond(HttpStatusCode.OK, spielerdaten)
        }

        post("/partien/{id}/start") {
            val id = uuidFromParameter(call.parameters["id"])
            val zustand = MauMauService.startePartie(id)
            call.respond(HttpStatusCode.OK, zustand)
        }

        post("/partien/{id}/ende") {
            val id = uuidFromParameter(call.parameters["id"])
            val zustand = MauMauService.beendePartie(id)
            call.respond(HttpStatusCode.OK, zustand)
        }

        // Create user
        post("/partie-spieler") {
            val createPartieSpieler = call.receive<CreatePartieSpieler>()
            MauMauService.verbindePartieUndSpieler(createPartieSpieler)
            call.respond(HttpStatusCode.Created)
        }
//        }
    }
}