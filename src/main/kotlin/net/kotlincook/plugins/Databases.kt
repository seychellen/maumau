package net.kotlincook.plugins

import io.ktor.server.application.*
import net.kotlincook.repo.PartieRepository
import net.kotlincook.repo.PartieSpielerRepository
import net.kotlincook.repo.SpielerRepository
import net.kotlincook.repo.SpielzugRepository
import org.jetbrains.exposed.sql.Database

fun Application.configureDatabases() {

    val database = Database.connect(
        url = environment.config.property("postgres.url").getString(),
        user = environment.config.property("postgres.user").getString(),
        password = environment.config.property("postgres.password").getString(),
        driver = "org.postgresql.Driver"
    )

    SpielzugRepository.create(database)
    SpielerRepository.create(database)
    SpielzugRepository.create(database)
    PartieRepository.create(database)
    PartieSpielerRepository.create(database)
}
