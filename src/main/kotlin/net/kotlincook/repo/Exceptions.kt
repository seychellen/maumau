package net.kotlincook.repo

import net.kotlincook.model.*
import kotlin.reflect.KClass

class EntityNotExistsException(id: Any, clazz: KClass<*>): RuntimeException(
    "No entity of type ${clazz.simpleName} found for id ${id}")

class EntityRelationConflict(id1: Any, clazz1: KClass<*>, id2: Any, clazz2: KClass<*>, message: String?): RuntimeException(
    "There is a conflict between entity of type ${clazz1.simpleName} and id ${id1} and " +
            "of type ${clazz2.simpleName} and id ${id2}:  ${message?: "no more info"}.")

class KartenortNotInKartenverteilungException(kartenort: Kartenort, keys: Set<Kartenort>):
    IllegalStateException("Kartenort ${kartenort} ist nicht enthalten in ${keys}.")

class NotEnoughKartenException(): IllegalStateException("Nicht genug Karten")

open class InvalidSpielzugException(spielzug: SpielzugInterface, kartenverteilung: Kartenverteilung, message: String? = ""):
    IllegalArgumentException("Ungueltiger Spielzug ${spielzug} auf Kartenverteilung ${kartenverteilung}" +
            if (message.isNullOrEmpty()) "" else ": $message"
    )

class VoidSpielzugException(spielzug: SpielzugInterface, kartenverteilung: Kartenverteilung):
    InvalidSpielzugException(spielzug, kartenverteilung,
        "Der Spielzug ist weder Ablegezug noch Ziehzug noch Kartengeberzug.")

class ElementAlreadyExistsException(message: String): RuntimeException(message)

class PartieAlreadyFinishedException(partie: Partie):
    IllegalArgumentException("Partie mit id ${partie.id} ist bereits beendet.")

class PartieAlreadyStartedException(partie: Partie):
    IllegalArgumentException("Partie mit id ${partie.id} ist bereits gestartet.")

class PartieNotYetStartedException(partie: Partie):
    IllegalArgumentException("Partie mit id ${partie.id} wurde noch nicht gestartet.")