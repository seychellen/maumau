package net.kotlincook.repo

import CreatePartieSpieler
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

abstract class PartieSpielerRepository(database: Database) {

    companion object {
        lateinit var instant: PartieSpielerRepository
        fun create(database: Database) {
            instant = object:PartieSpielerRepository(database){}
        }
    }

    object PartieSpielerTable : Table() {
        val partieId = reference("partieId", PartieRepository.PartieTable)
        val spielerId = reference("spielerId", SpielerRepository.SpielerTable)
        override val primaryKey = PrimaryKey(spielerId, partieId, name = "PK_PartieSpielerTable")
    }

    init {
        transaction(database) {
            SchemaUtils.create(PartieSpielerTable)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }


//    suspend fun create2(createPartieSpieler: CreatePartieSpieler) = dbQuery {
//        val partieDAO = PartieRepository.PartieDAO[createPartieSpieler.partieId]
//        val spielerDAO = SpielerRepository.SpielerDAO[createPartieSpieler.spielerId]
//        if (partieDAO == null) {
//            throw EntityNotExistsException(createPartieSpieler.partieId, PartieRepository.PartieDAO::class)
//        }
//        if (spielerDAO == null) {
//            throw EntityNotExistsException(createPartieSpieler.spielerId, SpielerRepository.SpielerDAO::class)
//        }
//
//        transaction {
//            partieDAO.spieler = SizedCollection(listOf(spielerDAO))
//        }
//    }

    suspend fun create(createPartieSpieler: CreatePartieSpieler) = dbQuery {
        PartieSpielerTable.insert {
            it[partieId] = createPartieSpieler.partieId
            it[spielerId] = createPartieSpieler.spielerId
        }
    }

}
