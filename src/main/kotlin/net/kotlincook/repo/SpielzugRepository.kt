package net.kotlincook.repo

import kotlinx.coroutines.Dispatchers
import kotlinx.datetime.toKotlinInstant
import kotlinx.serialization.Serializable
import net.kotlincook.model.*
import net.kotlincook.repo.SpielzugRepository.SpielzugTable.zugnummer
import net.kotlincook.util.UUIDSerializer
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant
import java.util.*

abstract class SpielzugRepository(database: Database) {

    companion object {
        lateinit var instant: SpielzugRepository
        fun create(database: Database) {
            instant = object : SpielzugRepository(database) {}
        }
    }

    // val mapper = ShapeShiftBuilder().build()

    class SpielzugDAO(id: EntityID<Int>) : IntEntity(id) {
        companion object : EntityClass<Int, SpielzugDAO>(SpielzugTable)

        @Serializable(UUIDSerializer::class)
        var partieId by SpielzugTable.partieId
        var karte by SpielzugTable.karte
        var von by SpielzugTable.von
        var nach by SpielzugTable.nach
        var zugnummer by SpielzugTable.zugnummer
        var wunschfarbe by SpielzugTable.wunschfarbe
        var erstellt by SpielzugTable.erstellt
    }

    object SpielzugTable : IntIdTable("spielzuege") {
        val partieId = uuid("partieId")
            .references(PartieRepository.PartieTable.id)
        val karte = varchar("karte", length = 10).nullable()
        val von = integer("von")
        val nach = integer("nach")
        val zugnummer = integer("zugnummer")
        val wunschfarbe = varchar("wunschfarbe", length = 64).nullable()
        val erstellt = timestamp("erstellt")

        init {
            uniqueIndex("unique_partieId_zugnummer", partieId, zugnummer)
        }
    }

    init {
        transaction(database) {
            SchemaUtils.create(SpielzugTable)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    suspend fun create(spielzug: CreateSpielzug): Int = dbQuery {
        val spielzuege = find(spielzug.partieId)
        SpielzugDAO.new {
            this.partieId = spielzug.partieId
            this.von = spielzug.von.toInt()
            this.nach = spielzug.nach.toInt()
            this.karte = spielzug.karte?.toString()
            this.wunschfarbe = spielzug.wunschfarbe?.toString()
            this.zugnummer = spielzuege.size
            this.erstellt = Instant.now()
        }.zugnummer
    }

    suspend fun find(partieId: UUID) = dbQuery {
        SpielzugDAO.find {
            SpielzugTable.partieId eq partieId
        }.orderBy(zugnummer to SortOrder.ASC)
            .map {
                Spielzug(
                    partieId = it.partieId,
                    karte = Karte.fromString(it.karte),
                    von = Kartenort.fromInt(it.von),
                    nach = Kartenort.fromInt(it.nach),
                    zugnummer = it.zugnummer,
                    wunschfarbe = FarbeEnum.fromString(it.wunschfarbe),
                    erstellt = it.erstellt.toKotlinInstant()
                )
            }
    }

    suspend fun read(partieId: UUID) = dbQuery {
        find(partieId).also {
            if (it.isEmpty()) {
                throw EntityNotExistsException(partieId, SpielzugDAO::class)
            }
        }
    }
}
