package net.kotlincook.repo

import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.Serializable
import net.kotlincook.model.CreateSpieler
import net.kotlincook.model.Spieler
import net.kotlincook.util.UUIDSerializer
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*


abstract class SpielerRepository(database: Database) {

    companion object {
        lateinit var instant: SpielerRepository
        fun create(database: Database) {
            instant = object : SpielerRepository(database) {}
        }
    }

    class SpielerDAO(id: EntityID<UUID>) : UUIDEntity(id) {
        companion object : EntityClass<UUID, SpielerDAO>(SpielerTable)

        @Serializable(UUIDSerializer::class)
        var name by SpielerTable.name
    }

    object SpielerTable : UUIDTable() {
        val name = varchar("name", length = 50)
    }

    init {
        transaction(database) {
            SchemaUtils.create(SpielerTable)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    suspend fun create(spieler: CreateSpieler): EntityID<UUID> = dbQuery {
        SpielerTable.insert {
            it[name] = spieler.name
        }[SpielerTable.id]
    }


    suspend fun read(id: UUID) = dbQuery {
        val spielerDAO = SpielerDAO[id]
        Spieler(spielerDAO.id.value, spielerDAO.name)
    }


    suspend fun update(id: UUID, spieler: CreateSpieler) {
        dbQuery {
            SpielerTable.update({ SpielerTable.id eq id }) {
                it[name] = spieler.name
            }
        }
    }

    suspend fun delete(id: UUID) {
        dbQuery {
            SpielerTable.deleteWhere { SpielerTable.id.eq(id) }
        }
    }
}