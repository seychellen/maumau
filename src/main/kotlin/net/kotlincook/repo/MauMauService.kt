package net.kotlincook.repo

import CreatePartieSpieler
import kotlinx.datetime.Clock
import net.kotlincook.model.*
import net.kotlincook.model.Kartenort.*
import java.lang.IllegalStateException
import java.util.*

object MauMauService {

    suspend fun erstelleSpieler(spieler: CreateSpieler): UUID {
        return SpielerRepository.instant.create(spieler).value
    }

    suspend fun ladeSpieler(id: UUID): Spieler {
        return SpielerRepository.instant.read(id)
    }

    suspend fun erstellePartie(): UUID {
        return PartieRepository.instant.create().value
    }

    suspend fun ladePartie(id: UUID): Partie {
        return PartieRepository.instant.read(id)
    }

    suspend fun ladeAktivePartien(): List<Partie> {
        return PartieRepository.instant.aktive()
    }

    suspend fun startePartie(partieId: UUID): Partie {
        val partie = PartieRepository.instant.read(partieId)
        if (partie.ende != null) throw PartieAlreadyFinishedException(partie)
        if (partie.start != null) throw PartieAlreadyStartedException(partie)

        val spielzuege = teileKartenAus(partie)
        spielzuege.forEach {
            SpielzugRepository.instant.create(it)
        }
        PartieRepository.instant.update(
            partie.copy(start = Clock.System.now())
        )
        return PartieRepository.instant.read(partieId)
    }

    suspend fun beendePartie(partieId: UUID): Partie {
        val partie = PartieRepository.instant.read(partieId)
        validatePartieZustand(partie)
        PartieRepository.instant.update(
            partie.copy(ende = Clock.System.now())
        )
        return PartieRepository.instant.read(partieId)
    }

    suspend fun zustandVonPartie(partieId: UUID): Zustand {
        val partie = PartieRepository.instant.read(partieId)
        return SpielzugRepository.instant.read(partieId)
            .fold(zustandInitial(partie)) { zustand, spielzug ->
                zustand.fuehreSpielzugAus(spielzug)
            }
    }

    suspend fun verbindePartieUndSpieler(createPartieSpieler: CreatePartieSpieler) {
        PartieSpielerRepository.instant.create(createPartieSpieler)
    }

    suspend fun ladeSpielzuege(partieId: UUID): List<Spielzug> {
        return SpielzugRepository.instant.read(partieId)
    }

    suspend fun macheSpielzug(spielzug: CreateSpielzug): Int {
        var zustand = zustandVonPartie(spielzug.partieId)
        if (zustand.ziehstapel.isEmpty()) {
            umverteilen(zustand.partie.id).forEach {
                SpielzugRepository.instant.create(it)
                zustand = zustand.fuehreSpielzugAus(it)
            }
        }
        validatePartieZustand(zustand.partie)

        val spielzugMod = zustand.ergaenzeZeihkarte(spielzug)
        if (zustand.fuehreSpielzugAus(spielzugMod).istSpielBeendet()) {
            beendePartie(zustand.partie.id)
        }
        return SpielzugRepository.instant.create(spielzugMod)
    }

    private fun validatePartieZustand(partie: Partie) {
        if (partie.start == null) throw PartieNotYetStartedException(partie)
        if (partie.ende != null) throw PartieAlreadyFinishedException(partie)
    }

    private fun neuesKartenspiel(): Kartenbuendel {
        return Kartenbuendel(FarbeEnum.values().flatMap { farbe ->
            WertEnum.values().map { wert -> Karte(farbe, wert) }
        })
    }

    private fun neuesGemischtesSpiel(): Kartenbuendel {
        val karten = neuesKartenspiel().karten.toMutableList()
        val random = Random(System.currentTimeMillis())
        for (i in 0..49) {
            val index = random.nextInt(karten.size)
            karten.add(karten.removeAt(index))
        }
        return Kartenbuendel(karten.toList())
    }



    private fun zustandInitial(partie: Partie): Zustand {
        if (partie.spieler.isEmpty()) {
            throw IllegalArgumentException("The set of Spieler is empty in ${partie}")
        }
        val kartenverteilung = mutableMapOf<Kartenort, Kartenbuendel>()
        kartenverteilung[Ziehstapel] = Kartenbuendel()
        kartenverteilung[Ablagestapel] = Kartenbuendel()
        kartenverteilung[Kartengeber] = neuesGemischtesSpiel()
        for (i in 0 until partie.spieler.size) {
            kartenverteilung[Spielerposition(i)] = Kartenbuendel()
        }
        return Zustand(kartenverteilung, true, ZiehPflicht(1, 0), null, Spielerposition(0), partie)
    }

    private fun teileKartenAus(partie: Partie, anzKartenProPerson: Int = 5): List<CreateSpielzug> {
        // Zuerst jeweils anzKartenProPerson Karten an die Spieler reihum:
        var zustand = zustandInitial(partie)
        val spielzuege = ArrayList<CreateSpielzug>()
        (0 until anzKartenProPerson).forEach {
            for (position in 0 until partie.spieler.size) {
                val spielzug = CreateSpielzug(
                    partie.id,
                    zustand.kartengeberKarte() ?: throw NotEnoughKartenException(),
                    Kartengeber,
                    Spielerposition(position)
                )
                spielzuege += spielzug
                zustand = zustand.fuehreSpielzugAus(spielzug)
            }
        }

        // Eine Karte schon mal auf den Ablagestapel:
        val spielzug = CreateSpielzug(
            partie.id,
            zustand.kartengeberKarte() ?: throw NotEnoughKartenException(),
            Kartengeber,
            Ablagestapel
        )
        spielzuege += spielzug
        zustand = zustand.fuehreSpielzugAus(spielzug)

        // Den Rest (mindestens eine) auf den Ziehstapel:
        if (zustand.kartengeberKarte() == null) {
            throw NotEnoughKartenException()
        }
        while (true) {
            val spielzg = CreateSpielzug(
                partie.id,
                zustand.kartengeberKarte() ?: break,
                Kartengeber,
                Ziehstapel
            )
            spielzuege += spielzg
            zustand = zustand.fuehreSpielzugAus(spielzg)
        }
        return spielzuege
    }

    private suspend fun umverteilen(partieId: UUID): List<CreateSpielzug> {
        val zustand = zustandVonPartie(partieId)
        val spielzuege = ArrayList<CreateSpielzug>()

        zustand.ablagestapel.karten.dropLast(1).forEach {
            val spielzug = CreateSpielzug(
                partieId,
                it,
                Ablagestapel,
                Ziehstapel
            )
            spielzuege += spielzug
        }
        return spielzuege
    }

    suspend fun spielerdaten(partieId: UUID, spielerId: UUID): Spielerdaten {
        val partie = ladePartie(partieId)
        val idx = partie.spieler.indexOfFirst { spieler -> spieler.id == spielerId }
        if (idx < 0) throw IllegalArgumentException("FIXME")

        val zustand = zustandVonPartie(partieId)
        val spielerposition = Spielerposition(idx)
        val spielername = zustand.spieler(spielerposition).name
        val amSpiel = spielerposition == zustand.aktSpielerposition
        val kartenbuendel = zustand.kartenverteilung[spielerposition]
            ?: throw IllegalStateException("FIXME")
        val spielerMitLetzterKarte = zustand.kartenverteilung
            .filter { (kartenort, _) -> kartenort is Spielerposition}
            .filter { (_, kartenbuendel) -> kartenbuendel.size() == 1}
            .map { (kartenort, _) -> zustand.spieler(kartenort as Spielerposition).name }

        val zuZiehen = if (amSpiel) zustand.ziehPflicht.bei7nochZuZiehen() else null

        return Spielerdaten(
            kartenbuendel,
            zustand.ablagestapelKarte(),
            spielername,
            amSpiel,
            zustand.imUhrzeigensinn,
            zustand.wunschfarbe,
            zuZiehen,
            spielerMitLetzterKarte,
            null)
    }

    suspend fun ablagestapel(partieId: UUID): Kartenbuendel {
        val zustand = zustandVonPartie(partieId)
        return zustand.ablagestapel
    }

}

