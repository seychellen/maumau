package net.kotlincook.repo

import dev.krud.shapeshift.ShapeShiftBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import kotlinx.serialization.Serializable
import net.kotlincook.model.Partie
import net.kotlincook.model.Spieler
import net.kotlincook.util.UUIDSerializer
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.util.*


abstract class PartieRepository(database: Database) {

    companion object {
        lateinit var instant: PartieRepository
        fun create(database: Database) {
            instant = object : PartieRepository(database) {}
        }
    }

    val mapper = ShapeShiftBuilder().build()

    class PartieDAO(id: EntityID<UUID>) : UUIDEntity(id) {
        companion object : EntityClass<UUID, PartieDAO>(PartieTable)

        @Serializable(UUIDSerializer::class)
        var start by PartieTable.start
        var ende by PartieTable.ende
        var spieler by SpielerRepository.SpielerDAO via PartieSpielerRepository.PartieSpielerTable
    }

    object PartieTable : UUIDTable() {
        val start = timestamp("start").nullable()
        val ende = timestamp("ende").nullable()
    }

    init {
        transaction(database) {
            SchemaUtils.create(PartieTable)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    suspend fun create(): EntityID<UUID> = dbQuery {
        PartieDAO.new {
        }.id
    }

    suspend fun aktive(): List<Partie> = dbQuery {
        PartieDAO.find {
            PartieTable.ende.isNull().and(PartieTable.start.isNotNull())
        }.map { p -> Partie(
                p.id.value,
                p.start?.toKotlinInstant(),
                null,
                p.spieler.map { s -> Spieler(s.id.value, s.name) })
        }
    }

    suspend fun read(id: UUID): Partie {
        return dbQuery {
            val partieDAO = PartieDAO[id]
            Partie(
                partieDAO.id.value,
                partieDAO.start?.toKotlinInstant(),
                partieDAO.ende?.toKotlinInstant(),
                partieDAO.spieler.map { spielerDAO ->
                    Spieler(spielerDAO.id.value, spielerDAO.name)
                }
            )
        }
    }

    suspend fun update(partie: Partie): Int {
        return dbQuery {
            PartieTable.update({ PartieTable.id eq partie.id }) {
                it[start] = partie.start?.toJavaInstant()
                it[ende] = partie.ende?.toJavaInstant()
            }
        }
    }

//    fun read(id: UUID): Partie {
//        return find(id) ?: throw EntityNotFoundException(id, PartieDAO::class)
//    }
//
//    suspend fun delete(id: UUID) {
//        dbQuery {
//            Spieler.deleteWhere { Spieler.id.eq(id) }
//        }
//    }
}