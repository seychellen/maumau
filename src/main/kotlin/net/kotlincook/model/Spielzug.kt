package net.kotlincook.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import net.kotlincook.util.UUIDSerializer
import java.util.*

interface SpielzugInterface {

    val partieId: UUID
    val karte: Karte?
    val von: Kartenort
    val nach: Kartenort
    val wunschfarbe: FarbeEnum?
}

@Serializable
data class Spielzug(
    @Serializable(UUIDSerializer::class)
    override val partieId: UUID,
    override val karte: Karte?,
    override val von: Kartenort,
    override val nach: Kartenort,
    val zugnummer: Int,
    override val wunschfarbe: FarbeEnum?,
    val erstellt: Instant
) : SpielzugInterface

@Serializable
data class CreateSpielzug(
    @Serializable(UUIDSerializer::class)
    override val partieId: UUID,
    override val karte: Karte?,
    override val von: Kartenort,
    override val nach: Kartenort,
    override val wunschfarbe: FarbeEnum? = null
) : SpielzugInterface


/**
 * Falls von oder nach ist eine Spielerposition ist, wird diese geliefert,
 * sonst null
 */
fun SpielzugInterface.spielerposition(): Kartenort.Spielerposition? {
    return when {
        von is Kartenort.Spielerposition -> von as Kartenort.Spielerposition
        nach is Kartenort.Spielerposition -> nach as Kartenort.Spielerposition
        else -> null
    }
}

fun SpielzugInterface.isAblegezug() =
    (von is Kartenort.Spielerposition && nach == Kartenort.Ablagestapel)

/**
 * Gibt an, ob es ich um einen Kartenzug eines Spieles vom Ziehstapel handelt
 */
fun SpielzugInterface.isZiehzug() =
    (von == Kartenort.Ziehstapel && nach is Kartenort.Spielerposition)

/**
 * Ein Pseudozug, der angibt, dass man gerade nicht ablegen kann.
 * @return true, falls es sich um einen "KannNichtZug" handelt.
 */
fun SpielzugInterface.isKannNichtZug() = karte == null

fun SpielzugInterface.isKartengeberzug() = von == Kartenort.Kartengeber

fun SpielzugInterface.isUmverteilzug() = von == Kartenort.Ablagestapel && nach == Kartenort.Ziehstapel

fun SpielzugInterface.isKartengeberAblegezug() = von == Kartenort.Kartengeber && nach == Kartenort.Ablagestapel
