package net.kotlincook.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(KarteSerializer::class)
data class Karte(val farbe: FarbeEnum, val wert: WertEnum) {

    constructor(pair: Pair<FarbeEnum, WertEnum>) : this(pair.first, pair.second)
    constructor(karteAsStr: String) : this(pairFromString(karteAsStr))

    companion object {
        val KARO_7 = Karte(FarbeEnum.KARO, WertEnum.Z7)
        val KARO_8 = Karte(FarbeEnum.KARO, WertEnum.Z8)
        val KARO_9 = Karte(FarbeEnum.KARO, WertEnum.Z9)
        val KARO_10 = Karte(FarbeEnum.KARO, WertEnum.Z10)
        val KARO_B = Karte(FarbeEnum.KARO, WertEnum.B)
        val KARO_D = Karte(FarbeEnum.KARO, WertEnum.D)
        val KARO_K = Karte(FarbeEnum.KARO, WertEnum.K)
        val KARO_A = Karte(FarbeEnum.KARO, WertEnum.A)

        val HERZ_7 = Karte(FarbeEnum.HERZ, WertEnum.Z7)
        val HERZ_8 = Karte(FarbeEnum.HERZ, WertEnum.Z8)
        val HERZ_9 = Karte(FarbeEnum.HERZ, WertEnum.Z9)
        val HERZ_10 = Karte(FarbeEnum.HERZ, WertEnum.Z10)
        val HERZ_B = Karte(FarbeEnum.HERZ, WertEnum.B)
        val HERZ_D = Karte(FarbeEnum.HERZ, WertEnum.D)
        val HERZ_K = Karte(FarbeEnum.HERZ, WertEnum.K)
        val HERZ_A = Karte(FarbeEnum.HERZ, WertEnum.A)

        val PIK_7 = Karte(FarbeEnum.PIK, WertEnum.Z7)
        val PIK_8 = Karte(FarbeEnum.PIK, WertEnum.Z8)
        val PIK_9 = Karte(FarbeEnum.PIK, WertEnum.Z9)
        val PIK_10 = Karte(FarbeEnum.PIK, WertEnum.Z10)
        val PIK_B = Karte(FarbeEnum.PIK, WertEnum.B)
        val PIK_D = Karte(FarbeEnum.PIK, WertEnum.D)
        val PIK_K = Karte(FarbeEnum.PIK, WertEnum.K)
        val PIK_A = Karte(FarbeEnum.PIK, WertEnum.A)

        val KREUZ_7 = Karte(FarbeEnum.KREUZ, WertEnum.Z7)
        val KREUZ_8 = Karte(FarbeEnum.KREUZ, WertEnum.Z8)
        val KREUZ_9 = Karte(FarbeEnum.KREUZ, WertEnum.Z9)
        val KREUZ_10 = Karte(FarbeEnum.KREUZ, WertEnum.Z10)
        val KREUZ_B = Karte(FarbeEnum.KREUZ, WertEnum.B)
        val KREUZ_D = Karte(FarbeEnum.KREUZ, WertEnum.D)
        val KREUZ_K = Karte(FarbeEnum.KREUZ, WertEnum.K)
        val KREUZ_A = Karte(FarbeEnum.KREUZ, WertEnum.A)

        fun fromString(karteAsStr: String?) = if (karteAsStr == null) null else Karte(karteAsStr)
        fun pairFromString(karteAsStr: String): Pair<FarbeEnum, WertEnum> {
            val parts = karteAsStr.split("_")
            return Pair(FarbeEnum.valueOf(parts[0]), WertEnum.fromString(parts[1]))
        }
    }
    override fun toString() = "${farbe}_${wert}"
}


object KarteSerializer : KSerializer<Karte> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Karte", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: Karte) {
        encoder.encodeString(value.toString())
    }

    override fun deserialize(decoder: Decoder): Karte {
        return (Karte(decoder.decodeString()))
    }
}