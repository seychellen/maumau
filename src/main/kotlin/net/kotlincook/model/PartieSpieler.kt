import kotlinx.serialization.Serializable
import net.kotlincook.util.UUIDSerializer
import java.util.UUID
@Serializable
data class CreatePartieSpieler(
    @Serializable(UUIDSerializer::class)
    val partieId: UUID,
    @Serializable(UUIDSerializer::class)
    val spielerId: UUID
)