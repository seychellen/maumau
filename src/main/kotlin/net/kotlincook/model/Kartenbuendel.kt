package net.kotlincook.model

import kotlinx.serialization.Serializable
import net.kotlincook.repo.ElementAlreadyExistsException

/**
 * Kann jede Sammlung von Karten sein, z.B. die Karten, die ein Spieler
 * auf der Hand zu haben oder die Menge der Karten des Ziehstapels oder
 * des Ablagestapels.
 */
@Serializable
data class Kartenbuendel(val karten: List<Karte>) {
    constructor(vararg karten: Karte) : this(karten.toList())

    fun isEmpty() = karten.isEmpty()
    fun size() = karten.size
    fun add(karte: Karte): Kartenbuendel {
        if (karten.contains(karte)) {
            throw ElementAlreadyExistsException("Karte ${karte} ist bereits enthalten in ${karten}")
        }
        return Kartenbuendel(karten + karte)
    }

    fun remove(karte: Karte): Kartenbuendel {
        if (!karten.contains(karte)) {
            throw NoSuchElementException("Karte ${karte} ist nicht enthalten in ${karten}")
        }
        return Kartenbuendel(karten.filter { it != karte })
    }

    fun remove() = Pair(karten.last(), Kartenbuendel(karten.dropLast(1)))

}
