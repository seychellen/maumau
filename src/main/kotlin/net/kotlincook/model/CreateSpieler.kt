package net.kotlincook.model

import kotlinx.serialization.Serializable
import net.kotlincook.util.UUIDSerializer
import java.util.*


@Serializable
data class Spieler(@Serializable(UUIDSerializer::class)
                   val id: UUID,
                   val name: String)
@Serializable
data class CreateSpieler(val name: String)