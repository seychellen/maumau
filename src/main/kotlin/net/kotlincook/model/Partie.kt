package net.kotlincook.model

import kotlinx.serialization.Serializable
import kotlinx.datetime.Instant
import net.kotlincook.util.UUIDSerializer
import java.util.UUID

@Serializable
data class Partie(
            @Serializable(UUIDSerializer::class)
            val id: UUID,
            val start: Instant?,
            val ende: Instant?,
            val spieler: List<Spieler>
) {
    fun istAktiv() = start != null && ende == null
}


