package net.kotlincook.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(KartenortSerializer::class)
sealed class Kartenort {
    @Serializable
    data class Spielerposition(val index: Int) : Kartenort() {
        override fun toInt() = index
    }
    data object Ablagestapel: Kartenort() {
        override fun toInt() = ABLAGESTAPEL_ID
    }
    data object Ziehstapel: Kartenort() {
        override fun toInt() = ZIEHSTAPEL_ID
    }
    data object Kartengeber: Kartenort() {
        override fun toInt() = KARTENGEBER_ID
    }
    abstract fun toInt(): Int
    companion object {
        const val ABLAGESTAPEL_ID = -1
        const val ZIEHSTAPEL_ID = -2
        const val KARTENGEBER_ID = -3
        fun fromInt(i: Int): Kartenort =
            when (i) {
                ABLAGESTAPEL_ID -> Ablagestapel
                ZIEHSTAPEL_ID -> Ziehstapel
                KARTENGEBER_ID -> Kartengeber
                else -> Spielerposition(i)
            }
    }
}

object KartenortSerializer : KSerializer<Kartenort> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Kartenort", PrimitiveKind.INT)

    override fun serialize(encoder: Encoder, value: Kartenort) {
        encoder.encodeInt(value.toInt())
    }
    override fun deserialize(decoder: Decoder): Kartenort {
        return (Kartenort.fromInt(decoder.decodeInt()))
    }
}