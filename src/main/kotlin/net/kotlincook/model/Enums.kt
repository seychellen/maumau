package net.kotlincook.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.lang.IllegalArgumentException

enum class FarbeEnum {
    KARO,
    HERZ,
    PIK,
    KREUZ;

    companion object {
        fun fromString(farbeAsStr: String?) =
            if (farbeAsStr == null) null else FarbeEnum.valueOf(farbeAsStr)
    }
}

enum class WertEnum(val str: String) {
    Z7("7"),
    Z8("8"),
    Z9("9"),
    Z10("10"),
    B("B"),
    D("D"),
    K("K"),
    A("A");

    override fun toString(): String {
        return str
    }

    companion object {
        fun fromString(str: String): WertEnum {
            return when (str) {
                "7" -> Z7
                "8" -> Z8
                "9" -> Z9
                "10" -> Z10
                "B" -> B
                "D" -> D
                "K" -> K
                "A" -> A
                else -> throw IllegalArgumentException("Invalid WertEnum ${str}")
            }
        }
    }
}

object WertEnumSerializer : KSerializer<WertEnum> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("WertEnum", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: WertEnum) {
        encoder.encodeString(value.str)
    }

    override fun deserialize(decoder: Decoder): WertEnum {
        return WertEnum.fromString(decoder.decodeString())
    }
}
