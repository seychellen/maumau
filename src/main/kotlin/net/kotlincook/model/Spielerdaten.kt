package net.kotlincook.model

import kotlinx.serialization.Serializable

@Serializable
data class Spielerdaten(
    val kartenbuendel: Kartenbuendel,
    val ablagestapelKarte: Karte,
    val spielerAmSpiel: String,
    val amSpiel: Boolean,
    val imUhrzeigersinn: Boolean,
    val wunschfarbe: FarbeEnum?,
    val zuZiehen: Int?,
    val spielerMitLetzterKarte: List<String>,
    val spielinfo: String?)