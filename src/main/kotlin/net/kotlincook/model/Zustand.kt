package net.kotlincook.model

import kotlinx.serialization.Serializable
import net.kotlincook.model.Kartenort.*
import net.kotlincook.repo.ElementAlreadyExistsException
import net.kotlincook.repo.InvalidSpielzugException
import net.kotlincook.repo.KartenortNotInKartenverteilungException
import net.kotlincook.repo.VoidSpielzugException

/**
 * Eine Kartenverteilung beinhaltet die Information, welcher Spieler welche
 * Karte hat, welche Karten der Kartengeber aktuell besitzt, welche Karten sich
 * auf dem Ziehstapel und welche sich auf dem Ablagestapel befinden.
 */
typealias Kartenverteilung = Map<Kartenort, Kartenbuendel>

@Serializable
/**
 * Die Ziehpficht drückt aus, wie viele Karten ein Spieler aktuell ziehen muss (zuZiehen)
 * und wieviel er bereits von diesen gezogen hat (gezogen)
 * @param zuZiehen
 * @param gezogen
 */
data class ZiehPflicht(val zuZiehen: Int, val gezogen: Int) {
    fun bei7nochZuZiehen(): Int? {
        return if (this == DEFAULT_ZIEH_PFLICHT) null
        else zuZiehen - gezogen
    }
}

val DEFAULT_ZIEH_PFLICHT = ZiehPflicht(1, 0)

@Serializable
data class Zustand(
    val kartenverteilung: Kartenverteilung,
    val imUhrzeigensinn: Boolean,
    val ziehPflicht: ZiehPflicht,
    val wunschfarbe: FarbeEnum?,
    val aktSpielerposition: Spielerposition,
    val partie: Partie
) {
    constructor(
        kartenverteilung: Kartenverteilung,
        imUhrzeigensinn: Boolean,
        aktSpielerposition: Spielerposition,
        partie: Partie
    ) :
            this(
                kartenverteilung,
                imUhrzeigensinn,
                DEFAULT_ZIEH_PFLICHT,
                null,
                aktSpielerposition,
                partie
            )
}


/**
 * Alle Karten des Ziehstapels
 */
val Zustand.ziehstapel: Kartenbuendel
    get() = kartenverteilung[Ziehstapel]
        ?: throw KartenortNotInKartenverteilungException(Ziehstapel, kartenverteilung.keys)

/**
 * Alle Karten des Kartengebers
 */
val Zustand.kartengeber: Kartenbuendel
    get() = kartenverteilung[Kartengeber]
        ?: throw KartenortNotInKartenverteilungException(Kartengeber, kartenverteilung.keys)

/**
 * Liefert die nächste Karte vom Kartengeber
 */
fun Zustand.kartengeberKarte(): Karte? {
    return try {
        kartengeber.karten.last()
    } catch (e: NoSuchElementException) {
        null
    }
}

/**
 * Liefert die oberste Karte des Ziehstapels
 */
fun Zustand.ziehstapelKarte(): Karte? {
    return try {
        ziehstapel.karten.last()
    } catch (e: NoSuchElementException) {
        null
    }
}

/**
 * Alle Karten des Ablagestapels
 */
val Zustand.ablagestapel: Kartenbuendel
    get() = kartenverteilung[Ablagestapel]
        ?: throw KartenortNotInKartenverteilungException(Ablagestapel, kartenverteilung.keys)

/**
 * Letzte abgelegte Karte des Ablagestapels
 */
fun Zustand.ablagestapelKarte(): Karte {
    return try {
        ablagestapel.karten.last()
    } catch (e: NoSuchElementException) {
        throw NoSuchElementException("Ablagestapel is empty")
    }
}

fun Zustand.spielerzahl() = partie.spieler.size

/**
 * Liefert den Spieler an der gewünschten Spielerposition
 * @param spielerposition Spielerposition
 * @return Spieler
 */
fun Zustand.spieler(spielerposition: Spielerposition): Spieler {
    return partie.spieler[spielerposition.index]
}

/**
 * Da der Spieler beim Ziehen einer Karte vom Ziehstapel vom Frontend
 * keine Karte mitgeteilt bekommt (null), wird diese für die DB zum Ziehzug
 * hinzugefügt. Dies ist aus Informationssicht zwar nicht unbedingt notwendig,
 * da man diese Karte auch bei der Rekonstruktion des Zustands eindeutig bestimmen
 * könnte, aber es ist schöner diese Information für die DB mit abzuspeichern.
 * @param spielzug Spielzug mit evtl. fehlender Karte
 * @return Spielzug mit ergänzter Karte vom Ziehstapel
 */
fun Zustand.ergaenzeZeihkarte(spielzug: CreateSpielzug): CreateSpielzug {
    return if (spielzug.isZiehzug() && spielzug.karte == null) {
        spielzug.copy(karte = ziehstapelKarte())
    } else spielzug
}

fun Zustand.istSpielBeendet(): Boolean {
    return kartenverteilung.filter {
        it.key is Spielerposition && it.value.isEmpty()
    }.isNotEmpty()
}

/**
 * Führt den Spielzug auf dem Zustand aus, ohne den Zustand selbst zu
 * verändern (immutable). Anstatt dessen wird ein neuer Zustand
 * zurückgegeben, der die Situation nach Ausführung des Zuges beschreibt.
 * @param spielzug
 * @return Zustand
 */
fun Zustand.fuehreSpielzugAus(spielzug: SpielzugInterface): Zustand {

    pruefeSpielerposition(spielzug)
    val kartenverteilungNeu = kartenverteilungNeu(spielzug)
    val ziehPflichtNeu = ziehPflichtNeu(spielzug)
    val wunschfarbeNeu = wunschfarbeNeu(spielzug)
    pruefeSpielzug(spielzug)

    return Zustand(
        kartenverteilungNeu,
        imUhrzeigersinnNeu(spielzug), ziehPflichtNeu, wunschfarbeNeu,
        spielerPositionNeu(spielzug), partie
    )
}

/**
 * Ermittelt eine neue Kartenverteilung basierend auf dem aktuellen Zustand, falls
 * der Zug <code>spielzug</code> ausgeführt würde. Der Zustand selbst wird dabei
 * nicht verändert.
 * @param spielzug der Spielzug
 * @return die Kartenverteilung
 * @see Kartenverteilung
 */
private fun Zustand.kartenverteilungNeu(spielzug: SpielzugInterface): Kartenverteilung {

    if (spielzug.isKannNichtZug()) {
        return kartenverteilung
    } else {

        if (spielzug.isZiehzug()) {
            if (spielzug.karte != ziehstapelKarte()) {
                throw InvalidSpielzugException(
                    spielzug, kartenverteilung,
                    "Gezogene Karte ${spielzug.karte} ist nicht oberste Karte ${ziehstapelKarte()} vom Ziehstapel."
                )
            }
        }

        val buendelMinus = try {
            (kartenverteilung[spielzug.von]
                ?: throw KartenortNotInKartenverteilungException(spielzug.von, kartenverteilung.keys))
                .remove(spielzug.karte!!)
        } catch (e: NoSuchElementException) {
            throw InvalidSpielzugException(spielzug, kartenverteilung, e.message)
        }

        val buendelPlus = try {
            (kartenverteilung[spielzug.nach]
                ?: throw KartenortNotInKartenverteilungException(spielzug.nach, kartenverteilung.keys))
                .add(spielzug.karte!!)
        } catch (e: ElementAlreadyExistsException) {
            throw InvalidSpielzugException(spielzug, kartenverteilung, e.message)
        }

        return kartenverteilung.map { (kartenort, kartenbuendel) ->
            when (kartenort) {
                spielzug.von -> spielzug.von to buendelMinus
                spielzug.nach -> spielzug.nach to buendelPlus
                else -> kartenort to kartenbuendel
            }
        }.toMap()
    }
}

private fun Zustand.imUhrzeigersinnNeu(spielzug: SpielzugInterface): Boolean {

    return when {
        spielzug.isKannNichtZug() -> return imUhrzeigensinn
        spielzug.isAblegezug() || spielzug.isKartengeberAblegezug() -> {
            if (spielzug.karte!!.wert == WertEnum.Z9) !imUhrzeigensinn else imUhrzeigensinn
        }

        spielzug.isZiehzug() -> imUhrzeigensinn
        spielzug.isKartengeberzug() -> imUhrzeigensinn
        spielzug.isUmverteilzug() -> imUhrzeigensinn
        else -> throw VoidSpielzugException(spielzug, kartenverteilung)
    }
}

private fun Zustand.ziehPflichtNeu(spielzug: SpielzugInterface): ZiehPflicht {

    return when {
        spielzug.isKannNichtZug() ->
            return if (ziehPflicht.zuZiehen == ziehPflicht.gezogen) DEFAULT_ZIEH_PFLICHT
            else throw InvalidSpielzugException(
                spielzug, kartenverteilung,
                "Spieler ${aktSpielerposition} muss erst eine Karte gezogen haben, bevor er ohne Ablegen weitergibt."
            )

        spielzug.isAblegezug() ->
            // ziehPflicht.zuZiehen == 1 ist der Normalfall (erste or-Bed.)
            // Es kann auch sein, dass ein Spieler 2,4,6,... gezogen hatte (zweite or-Bed.)
            // Auch dann kann er wieder eine Sieben ablegen
            if (ziehPflicht.zuZiehen == 1 || ziehPflicht.gezogen == ziehPflicht.zuZiehen) {
                if (spielzug.karte!!.wert == WertEnum.Z7) {
                    ZiehPflicht(2, 0)
                } else {
                    DEFAULT_ZIEH_PFLICHT
                }
            } else if (ziehPflicht.gezogen == 0) {
                if (spielzug.karte!!.wert == WertEnum.Z7) {
                    ZiehPflicht(ziehPflicht.zuZiehen + 2, 0)
                } else {
                    throw InvalidSpielzugException(
                        spielzug, kartenverteilung,
                        "Die Karte ${spielzug.karte} ist keine Sieben; es sind noch ${ziehPflicht.zuZiehen} Karten zu ziehen."
                    )
                }
            } else {
                throw InvalidSpielzugException(
                    spielzug, kartenverteilung,
                    "Es sind noch ${ziehPflicht.zuZiehen - ziehPflicht.gezogen} Karten zu ziehen."
                )
            }

        spielzug.isZiehzug() ->
            if (ziehPflicht.zuZiehen <= ziehPflicht.gezogen) {
                throw InvalidSpielzugException(spielzug, kartenverteilung,
                    "Es wurden bereits alle Karten gezogen.")
            } else {
                ZiehPflicht(ziehPflicht.zuZiehen, ziehPflicht.gezogen + 1)
            }

        spielzug.isKartengeberAblegezug() ->
            if (spielzug.karte!!.wert == WertEnum.Z7) {
                ZiehPflicht(2, 0)
            } else {
                DEFAULT_ZIEH_PFLICHT
            }

        spielzug.isKartengeberzug() -> ziehPflicht
        spielzug.isUmverteilzug() -> ziehPflicht
        else -> throw VoidSpielzugException(spielzug, kartenverteilung)
    }
}

private fun Zustand.wunschfarbeNeu(spielzug: SpielzugInterface): FarbeEnum? {

    return when {
        spielzug.isKannNichtZug() -> wunschfarbe
        spielzug.isAblegezug() -> {
            if (spielzug.karte!!.wert == WertEnum.B) {
                if (spielzug.wunschfarbe == null && kartenverteilung[spielzug.von]!!.size() > 1) {
                    throw InvalidSpielzugException(spielzug, kartenverteilung,
                        "Bei einem Bubenzug muss eine Wunschfarbe angegeben werden."
                    )
                } else {
                    spielzug.wunschfarbe
                }
            } else if (wunschfarbe == null) {
                return null
            } else if (spielzug.karte!!.farbe == wunschfarbe) {
                return null
            } else {
                throw InvalidSpielzugException(
                    spielzug, kartenverteilung,
                    "Die Karte ${spielzug.karte} bedient weder die Farbe ${wunschfarbe} noch ist sie selbst ein Bube."
                )
            }
        }

        spielzug.isZiehzug() -> wunschfarbe
        spielzug.isKartengeberzug() -> null
        spielzug.isUmverteilzug() -> wunschfarbe
        else -> throw VoidSpielzugException(spielzug, kartenverteilung)
    }
}

private fun Zustand.spielerPositionNeu(spielzug: SpielzugInterface): Spielerposition {
    val vorzeichen = if (imUhrzeigensinn) 1 else -1

    return when {
        spielzug.isKannNichtZug() -> {
            Spielerposition((aktSpielerposition.index + spielerzahl() + vorzeichen) % spielerzahl())
        }

        spielzug.isAblegezug() -> {
            val offset = if (spielzug.karte!!.wert == WertEnum.Z8) 2 else 1
            Spielerposition((aktSpielerposition.index + spielerzahl() + vorzeichen * offset) % spielerzahl())
        }

        spielzug.isZiehzug() -> aktSpielerposition
        spielzug.isKartengeberzug() -> Spielerposition(0)
        spielzug.isUmverteilzug() -> aktSpielerposition
        else -> throw VoidSpielzugException(spielzug, kartenverteilung)
    }
}

private fun Zustand.pruefeSpielerposition(spielzug: SpielzugInterface) {
    val spielerposition = spielzug.spielerposition()
    when {
        spielzug.isKartengeberzug() -> {}
        spielzug.isUmverteilzug() -> {}
        spielerposition == null -> {
            throw IllegalStateException("Ungültiger Zustand, Spielzug: $spielzug")
        }
        spielerposition ==  aktSpielerposition -> {}
        else -> throw InvalidSpielzugException(spielzug, kartenverteilung,
            "Spieler ${spieler(spielerposition)} ist nicht am Spiel.")
    }
}


private fun Zustand.pruefeSpielzug(spielzug: SpielzugInterface) {
    when {
        spielzug.isKannNichtZug() -> {}
        spielzug.isAblegezug() -> {
            val ablagekarte = ablagestapelKarte()
            if (spielzug.karte!!.wert != WertEnum.B && ablagekarte.wert != WertEnum.B) {
                if (spielzug.karte!!.wert != ablagekarte.wert && spielzug.karte!!.farbe != ablagekarte.farbe) {
                    throw InvalidSpielzugException(
                        spielzug, kartenverteilung,
                        "Weder Farbe noch Wert von Spielkarte ${spielzug.karte} und Ablagekarte ${ablagekarte} stimmen überein."
                    )
                }
            }
        }

        spielzug.isZiehzug() -> {}
        spielzug.isKartengeberzug() -> {}
        spielzug.isUmverteilzug() -> {}
        else -> throw VoidSpielzugException(spielzug, kartenverteilung)
    }

}

