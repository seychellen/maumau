package net.kotlincook

import io.ktor.server.application.*
import net.kotlincook.plugins.*


fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)


fun Application.module() {
    configureHTTP()
    configureSerialization()
    configureDatabases()
    configureSockets()
    configureSecurity()
    configureRouting()
}
