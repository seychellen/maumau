package net.kotlincook.model

import kotlinx.datetime.Clock
import net.kotlincook.model.Karte.Companion.HERZ_10
import net.kotlincook.model.Karte.Companion.HERZ_9
import net.kotlincook.model.Karte.Companion.KARO_10
import net.kotlincook.model.Karte.Companion.KARO_7
import net.kotlincook.model.Karte.Companion.KARO_K
import net.kotlincook.model.Karte.Companion.KREUZ_10
import net.kotlincook.model.Karte.Companion.KREUZ_7
import net.kotlincook.model.Karte.Companion.KREUZ_8
import net.kotlincook.model.Karte.Companion.KREUZ_9
import net.kotlincook.model.Karte.Companion.PIK_7
import net.kotlincook.model.Karte.Companion.PIK_9
import net.kotlincook.model.Kartenort.*
import net.kotlincook.repo.InvalidSpielzugException
import java.util.*
import kotlin.test.*

class FuehreSpielzugAusTest {

    val spieler0 = Spieler(UUID.randomUUID(), "Fred")
    val spieler1 = Spieler(UUID.randomUUID(), "Till")
    val partie = Partie(UUID.randomUUID(), Clock.System.now(), null, listOf(spieler0, spieler1))
    val pos0 = Spielerposition(0)
    val pos1 = Spielerposition(1)

    @Test
    fun testKorrektesAblegenEinerKarteGleicherFarbe() {
        val kartenverteilung = mapOf(
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(KREUZ_9),
            pos1 to Kartenbuendel(PIK_9)
        )
        val zustand = Zustand(kartenverteilung, true, pos0, partie)
        val spielzug = CreateSpielzug(partie.id, KREUZ_9, pos0, Ablagestapel, null)
        val zustandActual = zustand.fuehreSpielzugAus(spielzug)

        val kartenverteilungExpected = mapOf(
            Ablagestapel to Kartenbuendel(KREUZ_10, KREUZ_9),
            pos0 to Kartenbuendel(),
            pos1 to Kartenbuendel(PIK_9)
        )

        assertEquals(kartenverteilungExpected, zustandActual.kartenverteilung)
        assertFalse(zustandActual.imUhrzeigensinn)
        assertEquals(DEFAULT_ZIEH_PFLICHT, zustandActual.ziehPflicht)
        assertEquals(pos1, zustandActual.aktSpielerposition)
        assertNull(zustandActual.wunschfarbe)
        assertEquals(partie, zustandActual.partie)
    }

    @Test
    fun testKartengeberLegtNeunAlsErsteKarte() {
        val kartenverteilung = mapOf(
            Kartengeber to Kartenbuendel(KARO_7, KREUZ_9),
            Ablagestapel to Kartenbuendel(),
            pos0 to Kartenbuendel(),
            pos1 to Kartenbuendel()
        )
        val zustand1 = Zustand(kartenverteilung, true, pos0, partie)
        val spielzug1 = CreateSpielzug(partie.id, KREUZ_9, Kartengeber, Ablagestapel, null)
        val zustand2 = zustand1.fuehreSpielzugAus(spielzug1)
        val spielzug2 = CreateSpielzug(partie.id, KARO_7, Kartengeber, pos0, null)
        val zustandActual = zustand2.fuehreSpielzugAus(spielzug2)

        val kartenverteilungExpected = mapOf(
            Kartengeber to Kartenbuendel(),
            Ablagestapel to Kartenbuendel(KREUZ_9),
            pos0 to Kartenbuendel(KARO_7),
            pos1 to Kartenbuendel()
        )

        assertEquals(kartenverteilungExpected, zustandActual.kartenverteilung)
        assertFalse(zustandActual.imUhrzeigensinn)
        assertEquals(DEFAULT_ZIEH_PFLICHT, zustandActual.ziehPflicht)
        assertEquals(pos0, zustandActual.aktSpielerposition)
        assertNull(zustandActual.wunschfarbe)
        assertEquals(partie, zustandActual.partie)
    }

    @Test
    fun testKorrektesAblegenEinerKarteMitGleichemWert() {
        val kartenverteilung = mapOf(
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(HERZ_10),
            pos1 to Kartenbuendel(PIK_9)
        )
        val zustand = Zustand(kartenverteilung, true, pos0, partie)
        val spielzug = CreateSpielzug(partie.id, HERZ_10, pos0, Ablagestapel, null)
        val zustandActual = zustand.fuehreSpielzugAus(spielzug)

        val kartenverteilungExpected = mapOf(
            Ablagestapel to Kartenbuendel(KREUZ_10, HERZ_10),
            pos0 to Kartenbuendel(),
            pos1 to Kartenbuendel(PIK_9)
        )

        assertEquals(kartenverteilungExpected, zustandActual.kartenverteilung)
        assertTrue(zustandActual.imUhrzeigensinn)
        assertEquals(DEFAULT_ZIEH_PFLICHT, zustandActual.ziehPflicht)
        assertEquals(pos1, zustandActual.aktSpielerposition)
        assertNull(zustandActual.wunschfarbe)
        assertEquals(partie, zustandActual.partie)
    }

    @Test
    fun testSpielerHatAblegekarteGarNicht() {
        val kartenverteilung = mapOf(
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(KREUZ_9)
        )
        val zustand = Zustand(kartenverteilung, true, pos0, partie)
        val spielzug = CreateSpielzug(partie.id, KREUZ_8, pos0, Ablagestapel, null)
        val e = assertFailsWith<InvalidSpielzugException> {
            zustand.fuehreSpielzugAus(spielzug)
        }
        assertContains(e.message!!, "Karte KREUZ_8 ist nicht enthalten in")
    }

    @Test
    fun testSpielerLegtUngueltigeKarteAb() {
        val kartenverteilung = mapOf(
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(HERZ_9)
        )
        val zustand = Zustand(kartenverteilung, true, pos0, partie)
        val spielzug = CreateSpielzug(partie.id, HERZ_9, pos0, Ablagestapel, null)

        val e = assertFailsWith<InvalidSpielzugException> {
            zustand.fuehreSpielzugAus(spielzug)
        }
        assertContains(e.message!!, "Weder Farbe noch Wert von Spielkarte")
    }


    @Test
    fun testSpielerMussZiehenBevorErKannNichtSagt() {
        val kartenverteilung = mapOf(
            Ziehstapel to Kartenbuendel(KARO_K),
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(HERZ_9)
        )
        val zustand = Zustand(kartenverteilung, true, pos0, partie)
        val kannNichtSpielzug = CreateSpielzug(partie.id, null, pos0, pos0, null)

        val e = assertFailsWith<InvalidSpielzugException> {
            zustand.fuehreSpielzugAus(kannNichtSpielzug)
        }
        assertContains(e.message!!, "muss erst eine Karte gezogen haben, bevor er ohne Ablegen weitergibt")

        val ziehSpielzug = CreateSpielzug(partie.id, KARO_K, Ziehstapel, pos0, null)
        val zustandActual1 = zustand.fuehreSpielzugAus(ziehSpielzug)

        val kartenverteilungExpected = mapOf(
            Ziehstapel to Kartenbuendel(),
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(HERZ_9, KARO_K)
        )
        assertEquals(kartenverteilungExpected, zustandActual1.kartenverteilung)
        assertEquals(pos0, zustandActual1.aktSpielerposition)
        assertEquals(ZiehPflicht(1, 1), zustandActual1.ziehPflicht)
        assertTrue(zustandActual1.imUhrzeigensinn)
        assertNull(zustandActual1.wunschfarbe)


        val zustandActual2 = zustandActual1.fuehreSpielzugAus(kannNichtSpielzug)
        assertEquals(kartenverteilungExpected, zustandActual2.kartenverteilung)
        assertEquals(pos1, zustandActual2.aktSpielerposition)
        assertEquals(DEFAULT_ZIEH_PFLICHT, zustandActual2.ziehPflicht)
        assertTrue(zustandActual1.imUhrzeigensinn)
        assertNull(zustandActual1.wunschfarbe)
    }


    @Test
    fun testSpielerSpieltSieben() {
        val kartenverteilung = mapOf(
            Ziehstapel to Kartenbuendel(HERZ_9, KARO_10, PIK_7, KREUZ_8),
            Ablagestapel to Kartenbuendel(KREUZ_10),
            pos0 to Kartenbuendel(KREUZ_7),
            pos1 to Kartenbuendel(HERZ_9)
        )
        // Spieler 0 spielt eine KREUZ_7:
        val zustand = Zustand(kartenverteilung, true, pos0, partie)
        val spielzug = CreateSpielzug(partie.id, KREUZ_7, pos0, Ablagestapel, null)
        val zustandActual1 = zustand.fuehreSpielzugAus(spielzug)

        assertEquals(ZiehPflicht(2, 0), zustandActual1.ziehPflicht)

        // Spieler 1 versucht eine Karte abzulegen, was fehlschlagen muss:
        val spielzug2 = CreateSpielzug(partie.id, HERZ_9, pos1, Ablagestapel, null)
        val e1 = assertFailsWith<InvalidSpielzugException> {
            zustandActual1.fuehreSpielzugAus(spielzug2)
        }
        assertContains(e1.message!!, "ist keine Sieben; es sind noch 2 Karten zu ziehen")

        // Spieler 1 zieht eine KREUZ_8:
        val ziehzug1 = CreateSpielzug(partie.id, KREUZ_8, Ziehstapel, pos1, null)
        val zustandActual2 = zustandActual1.fuehreSpielzugAus(ziehzug1)
        assertEquals(ZiehPflicht(2, 1), zustandActual2.ziehPflicht)

        // Spieler 1 versucht die KREUZ_8 abzulegen, was fehlschlagen muss:
        val spielzug3 = CreateSpielzug(partie.id, KREUZ_8, pos1, Ablagestapel, null)
        val e2 = assertFailsWith<InvalidSpielzugException> {
            zustandActual2.fuehreSpielzugAus(spielzug3)
        }
        assertContains(e2.message!!, "Es sind noch 1 Karten zu ziehen")

        // Spieler 1 zieht eine PIK_7:
        val ziehzug2 = CreateSpielzug(partie.id, PIK_7, Ziehstapel, pos1, null)
        val zustandActual3 = zustandActual2.fuehreSpielzugAus(ziehzug2)
        assertEquals(ZiehPflicht(2, 2), zustandActual3.ziehPflicht)

        // Variante a:
        // Spieler 1 legt die KREUZ_8 ab, was er jetzt darf:
        val spielzug4a = CreateSpielzug(partie.id, KREUZ_8, pos1, Ablagestapel, null)
        val zustandActual4a = zustandActual3.fuehreSpielzugAus(spielzug4a)

        // Spieler 1 ist immer noch dran wegen der Acht:
        assertEquals(ZiehPflicht(1, 0), zustandActual4a.ziehPflicht)
        assertEquals(pos1, zustandActual4a.aktSpielerposition)
        assertTrue(zustandActual4a.imUhrzeigensinn)

        val kartenverteilungExpecteda = mapOf(
            Ziehstapel to Kartenbuendel(HERZ_9, KARO_10),
            Ablagestapel to Kartenbuendel(KREUZ_10, KREUZ_7, KREUZ_8),
            pos0 to Kartenbuendel(),
            pos1 to Kartenbuendel(HERZ_9, PIK_7)
        )
        assertEquals(kartenverteilungExpecteda, zustandActual4a.kartenverteilung)

        // Variante b:
        // Spieler 1 legt PIK_7 ab, was er jetzt darf:
        val spielzug4b = CreateSpielzug(partie.id, PIK_7, pos1, Ablagestapel, null)
        val zustandActual4b = zustandActual3.fuehreSpielzugAus(spielzug4b)

        // Spieler 0 ist nun dran und hat zwei zu ziehen:
        assertEquals(ZiehPflicht(2, 0), zustandActual4b.ziehPflicht)
        assertEquals(pos0, zustandActual4b.aktSpielerposition)
        assertTrue(zustandActual4b.imUhrzeigensinn)

        val kartenverteilungExpectedb = mapOf(
            Ziehstapel to Kartenbuendel(HERZ_9, KARO_10),
            Ablagestapel to Kartenbuendel(KREUZ_10, KREUZ_7, PIK_7),
            pos0 to Kartenbuendel(),
            pos1 to Kartenbuendel(HERZ_9, KREUZ_8)
        )
        assertEquals(kartenverteilungExpectedb, zustandActual4b.kartenverteilung)
    }


}