# Verwenden Sie ein OpenJDK-Base-Image
FROM openjdk:17

RUN mkdir /app

# Fügen Sie den Projektordner zum Container hinzu
COPY build/libs/MauMau-all.jar /app/MauMau-all.jar

# Setzen Sie den Port, den Ihr Ktor-Backend verwendet
EXPOSE 8080:8080

# Führen Sie den Ktor-Server beim Start des Containers aus
ENTRYPOINT ["java", "-jar", "/app/MauMau-all.jar"]